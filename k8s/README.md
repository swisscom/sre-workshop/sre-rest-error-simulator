# rest error simulator k8s setup 

## k8s distribution

For this testsetup https://k3s.io/ is used

## rest error simulator backend service

The backend service is reachable under `rest-error.resterror.svc.cluster.local` within the cluster:
* REST endpoint:  `rest-error.resterror.svc.cluster.local:8080/best-tools`
* metrics endpoint: ` rest-error.resterror.svc.cluster.local:8080/metrics`




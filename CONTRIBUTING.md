# Contributing Guidelines

Thank you for your interest in contributing to this project! Here, we outline the guidelines to follow when contributing to this GitLab repository. Please take a moment to review this document before making any contributions.

## Creating Issues

If you encounter a bug, have a feature request, or need any assistance, please create an issue [here](../../issues). When creating an issue, please provide a clear and detailed description, along with any relevant information, steps to reproduce, and screenshots if applicable.

## Pull Requests

We welcome pull requests for bug fixes, new features, and improvements. Here's how to submit a pull request:

1. Fork the repository and create a new branch.
2. Make your changes or additions in the new branch.
3. Write appropriate tests if applicable.
4. Commit your changes with a clear and descriptive message.
5. Push your branch to your forked repository.
6. Submit a pull request to the `master` branch of this repository.

## Coding Guidelines

To ensure consistency and maintainability, please adhere to the following coding guidelines:

- Follow the established code style and formatting.
- Keep your code clean, organized, and readable.
- Comment your code when necessary, particularly on complex or non-intuitive sections.
- Write clear commit messages explaining the purpose and impact of your changes.

## Testing

If you're submitting code changes, please ensure that your changes are properly tested. Include relevant test cases to cover new features or bug fixes. Before submitting a pull request, run the existing test suite on your local environment to ensure that it passes.

## Code of Conduct

Please note that we have adopted a [Code of Conduct](CODE_OF_CONDUCT.md) which outlines our expectations for participants' behavior.

We appreciate your contributions and look forward to your pull requests! Thank you for helping to improve this project.
